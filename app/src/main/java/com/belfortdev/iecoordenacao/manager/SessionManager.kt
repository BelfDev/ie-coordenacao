package com.belfortdev.iecoordenacao.manager

import android.content.Context
import android.content.SharedPreferences
import com.belfortdev.iecoordenacao.data.model.domain.User

class SessionManager(context: Context) {

    companion object {
        const val USER_PREFS: String = "USER_PREFS"
        const val CURRENT_USER_ID: String = "CURRENT_USER_ID"
        const val CURRENT_USER_SENDER_NAME: String = "CURRENT_USER_SENDER_NAME"
        const val CURRENT_SESSION_ID: String = "CURRENT_SESSION_ID"
        const val LAST_USER_NAME: String = "LAST_USER_NAME"
    }

    val prefs: SharedPreferences = context.getSharedPreferences(USER_PREFS, 0)

    fun saveUserInfo(user: User, sessionId: String) {
        val editor = prefs.edit()
        with(editor) {
            putString(CURRENT_USER_ID, user.id)
            putString(CURRENT_USER_SENDER_NAME, user.senderName)
            putString(CURRENT_SESSION_ID, sessionId)
            putString(LAST_USER_NAME, user.username)
            commit()
        }
    }

    fun eraseUseInfo() {
        val editor = prefs.edit()
        with(editor) {
            putString(CURRENT_USER_ID, null)
            putString(CURRENT_USER_SENDER_NAME, null)
            commit()
        }
    }

    fun isUserLoggedIn(): Boolean = prefs.getString(CURRENT_USER_ID, null) != null

    fun getSessionId(): String = prefs.getString(CURRENT_SESSION_ID, null)

    fun getSenderName() = prefs.getString(CURRENT_USER_SENDER_NAME, null)

    fun getLastUserName() = prefs.getString(LAST_USER_NAME, "")

}