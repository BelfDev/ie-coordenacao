package com.belfortdev.iecoordenacao.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.belfortdev.iecoordenacao.R
import com.belfortdev.iecoordenacao.fragment.LoginFragment

class AuthenticationActivity : BaseActivity() {

    companion object {
        fun newActivityIntent(context: Context): Intent = Intent(context, AuthenticationActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(savedInstanceState == null) {
            replaceFragment(R.id.unique_fragment_container, LoginFragment.newInstance())
        }

    }

    override fun onBackPressed() {
        val exitIntent = Intent(Intent.ACTION_MAIN)
        exitIntent.addCategory(Intent.CATEGORY_HOME)
        exitIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(exitIntent)
    }

    override fun getActivityLayout() = R.layout.activity_authentication
}
