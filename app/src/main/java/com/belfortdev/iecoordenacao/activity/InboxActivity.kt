package com.belfortdev.iecoordenacao.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.belfortdev.iecoordenacao.R
import com.belfortdev.iecoordenacao.fragment.inbox.InboxFragment

class InboxActivity : BaseActivity() {

    companion object {
        fun newActivityIntent(context: Context): Intent = Intent(context, InboxActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupUpNavigationToolbar()

        if(savedInstanceState == null)
        {
            replaceFragment(R.id.unique_fragment_container, InboxFragment.newInstance())
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }

    override fun getActivityLayout() = R.layout.activity_inbox
}
