package com.belfortdev.iecoordenacao.activity

import android.app.ProgressDialog
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.appcompat.R
import kotlinx.android.synthetic.main.toolbar.*

abstract class BaseActivity : AppCompatActivity() {

    var loadingDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadingDialog = createLoadingDialog()
        setContentView(getActivityLayout())
        setSupportActionBar(toolbar)
    }

    protected fun replaceFragment(@IdRes containerViewId: Int, fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(containerViewId, fragment)
                .commitAllowingStateLoss()
    }

    protected fun setupUpNavigationToolbar() {
        val arrowDrawable = ContextCompat.getDrawable(this, R.drawable.abc_ic_ab_back_material)
        arrowDrawable?.setColorFilter(ContextCompat.getColor(this, com.belfortdev.iecoordenacao.R.color.lightGray), PorterDuff.Mode.SRC_ATOP)
        with(supportActionBar) {
            this?.setDisplayHomeAsUpEnabled(true)
            this?.setDisplayShowHomeEnabled(true)
            this?.setHomeAsUpIndicator(arrowDrawable)
        }
    }

    protected fun toggleLoading(shouldShow: Boolean) {
        if (shouldShow) loadingDialog?.show() else loadingDialog?.dismiss()
    }

    private fun createLoadingDialog(): ProgressDialog {
        val progress = ProgressDialog(this, com.belfortdev.iecoordenacao.R.style.AppCompatAlertDialogStyle)
        progress.setTitle("Carregando")
        progress.setMessage("Aguarde um instante...")
        progress.setCancelable(false)
        return progress
    }

    abstract fun getActivityLayout(): Int
}