package com.belfortdev.iecoordenacao.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.belfortdev.iecoordenacao.R
import com.belfortdev.iecoordenacao.data.model.domain.Message
import com.belfortdev.iecoordenacao.fragment.CompositionFragment


class MessageActivity : BaseActivity() {

    companion object {
        const val MESSAGE_READ = "MESSAGE_READ"

        fun newActivityIntent(context: Context): Intent = Intent(context, MessageActivity::class.java)

        fun newActivityFromMessageIntent(context: Context, message: Message): Intent {
            val intent = MessageActivity.newActivityIntent(context)
            intent.putExtra(MESSAGE_READ, message)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupUpNavigationToolbar()

        if (savedInstanceState == null) {
            val compositionFragment = CompositionFragment.newInstance()
            if (intent != null) {
                val data = intent.extras
                compositionFragment.setArguments(data)
            }
            replaceFragment(R.id.unique_fragment_container, compositionFragment)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.message_toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.send_menu_item -> return false
            R.id.discard_menu_item -> return false
        }
        return false
    }

    override fun getActivityLayout() = R.layout.activity_message

}
