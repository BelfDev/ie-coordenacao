package com.belfortdev.iecoordenacao.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import com.belfortdev.iecoordenacao.R
import com.belfortdev.iecoordenacao.fragment.CoordinatorPanelFragment
import com.belfortdev.iecoordenacao.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.fragment_login.*

class CoordinatorPanelActivity : BaseActivity() {

    companion object {
        val LOGIN_REQUEST = 1
        fun newActivityIntent(context: Context): Intent = Intent(context, CoordinatorPanelActivity::class.java)
    }

    private var userViewModel: UserViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.title = resources.getString(R.string.coordinator_panel_title)
//        setupToolbar()
        initViewModel()
    }

    override fun onStart() {
        super.onStart()
        verifyAuthentication()
    }

    private fun setupToolbar() {
        with(supportActionBar) {
            this?.setDisplayHomeAsUpEnabled(true)
            this?.setHomeAsUpIndicator(R.drawable.ic_menu)
        }
    }

    private fun initViewModel() {
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        userViewModel?.let { lifecycle.addObserver(it) }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.coordinator_toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.logout_menu_item -> onUserLogoutAttempt()
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == LOGIN_REQUEST && resultCode == RESULT_OK) {
            replaceFragment(R.id.unique_fragment_container, CoordinatorPanelFragment.newInstance())
        }
    }

    private fun onUserLogoutAttempt() {
        toggleLoading(true)
        userViewModel?.logUserOut()?.observe(this, Observer { isSuccessful ->
            toggleLoading(false)
            if (isSuccessful!!) {
                startActivityForResult(AuthenticationActivity.newActivityIntent(this), LOGIN_REQUEST)
            } else {
                val alert = createAlert()
                alert.setOnShowListener {
                    alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.vividRed))
                }
                alert.show()
            }
        })
    }

    private fun verifyAuthentication() {
        if (userViewModel?.shouldDisplayAuthentication()!!) {
            startActivityForResult(AuthenticationActivity.newActivityIntent(this), LOGIN_REQUEST)
        } else {
            replaceFragment(R.id.unique_fragment_container, CoordinatorPanelFragment.newInstance())
        }
    }

    private fun createAlert(): AlertDialog {
        val builder = AlertDialog.Builder(this)
        with(builder!!) {
            setTitle("Oops")
            setMessage("Não foi possível efetuar logout")
            setPositiveButton("Ok", onPositiveButtonPress())
        }
        return builder.create()
    }

    private fun onPositiveButtonPress(): DialogInterface.OnClickListener {
        return DialogInterface.OnClickListener { dialog, i ->
            userViewModel?.passwordInput = ""
            password_et.setText("")
            dialog.dismiss()
        }
    }

    override fun getActivityLayout() = R.layout.activity_coordinator_panel
}
