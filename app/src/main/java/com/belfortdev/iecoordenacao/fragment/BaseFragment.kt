package com.belfortdev.iecoordenacao.fragment

import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.belfortdev.iecoordenacao.R


abstract class BaseFragment : Fragment() {

    var loadingDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadingDialog = createLoadingDialog()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getFragmentLayoutResource(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    protected fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    protected fun View.showKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }

    protected fun toggleLoading(shouldShow: Boolean) {
        if (shouldShow) loadingDialog?.show() else loadingDialog?.dismiss()
    }

    private fun createLoadingDialog(): ProgressDialog {
        val progress = ProgressDialog(requireContext(), R.style.AppCompatAlertDialogStyle)
        progress.setTitle("Carregando")
        progress.setMessage("Aguarde um instante...")
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progress.setCancelable(false)
        return progress
    }

    protected fun showDialogAlert(title: String = "Oops", message: String, positiveClickListener: DialogInterface.OnClickListener = onPositiveButtonDefaultBehaviour()) {
        val alert = createAlert(title, message, positiveClickListener)
        alert.setOnShowListener {
            context?.let { ContextCompat.getColor(it, R.color.vividRed) }?.let { alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(it) }
        }
        alert.show()
    }

    private fun createAlert(title: String, message: String, positiveClickListener: DialogInterface.OnClickListener): AlertDialog {
        val builder = context?.let { AlertDialog.Builder(it) }
        with(builder!!) {
            setTitle(title)
            setMessage(message)
            setPositiveButton("Ok", positiveClickListener)
        }
        return builder.create()
    }

    private fun onPositiveButtonDefaultBehaviour(): DialogInterface.OnClickListener {
        return DialogInterface.OnClickListener { dialog, _ ->
            dialog.dismiss()
        }
    }

    abstract fun getFragmentLayoutResource(): Int
}