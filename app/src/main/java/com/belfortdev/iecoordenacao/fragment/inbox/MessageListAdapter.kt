package com.belfortdev.iecoordenacao.fragment.inbox

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.belfortdev.iecoordenacao.R
import com.belfortdev.iecoordenacao.data.model.domain.Message
import kotlinx.android.synthetic.main.list_item_message.view.*

class MessageListAdapter : RecyclerView.Adapter<MessageVH>() {

    private var messageList = emptyList<Message>()
    var interactor: MessageInteractor? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageVH {
        return MessageVH(LayoutInflater.from(parent?.context).inflate(R.layout.list_item_message, parent, false))
    }

    override fun getItemCount(): Int = messageList.size

    override fun onBindViewHolder(holder: MessageVH, position: Int) {
        holder?.bind(messageList[position])

        if (holder?.itemView != null)
            holder.itemView.message_card.setOnClickListener { view ->
                with(view) {
                    interactor?.onMessageCardClicked(messageList[position])
                }
            }
    }

    fun setData(data: List<Message>?) {
        messageList = data ?: messageList
        notifyDataSetChanged()
    }

    interface MessageInteractor {
        fun onMessageCardClicked(message: Message)
    }

}