package com.belfortdev.iecoordenacao.fragment.inbox

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.support.v4.content.ContextCompat
import android.support.v4.os.ConfigurationCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import com.belfortdev.iecoordenacao.R
import com.belfortdev.iecoordenacao.data.model.domain.Data
import com.belfortdev.iecoordenacao.data.model.domain.Message
import kotlinx.android.synthetic.main.list_item_message.view.*
import java.text.SimpleDateFormat
import java.util.*


class MessageVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(message: Message) {
        val recipient = interpretRecipient(message)
        itemView.recipient_tv.text = recipient
        itemView.sent_date_tv.text = formatDate(message.createdAt)
        itemView.message_preview_tv.text = message.content
        itemView.preview_thumb_tv.text = recipient.substring(0, 2).toUpperCase()
        setPreviewThumbBackground(message)
    }

    private fun interpretRecipient(message: Message): String {
        return when {
            message.grade != null -> message.grade
            message.className != null -> message.className
            else -> message.institute
        }
    }

    private fun formatDate(date: Date): String {
        val dateToCompare = Calendar.getInstance()
        val today = Calendar.getInstance()
        val yesterday = Calendar.getInstance()

        dateToCompare.time = date
        with (yesterday) {
            add(Calendar.DAY_OF_YEAR, -1)
            time
        }

        return when {
            isSameDay(today, dateToCompare) -> "Hoje"
            isSameDay(yesterday, dateToCompare) -> "Ontem"
            else -> {
                val locale = ConfigurationCompat.getLocales(itemView.resources.configuration).get(0)
                SimpleDateFormat("d 'de' MMM", locale).format(date)
            }
        }

    }

    private fun isSameDay(cal1: Calendar, cal2: Calendar): Boolean {
        return cal1.get(Calendar.YEAR) === cal2.get(Calendar.YEAR) && cal1.get(Calendar.DAY_OF_YEAR) === cal2.get(Calendar.DAY_OF_YEAR)
    }

    private fun setPreviewThumbBackground(messae: Message) {
        val background = itemView.preview_thumb_tv.background
        if (background is GradientDrawable) {
            background.setColor(ContextCompat.getColor(itemView.context, getColor(messae)))
        }
    }

    private fun getColor(message: Message): Int {
        return when {
            message.grade != null -> R.color.vividOrange
            message.className != null -> R.color.vividBurgundy
            else -> R.color.vividPurple
        }
    }

}