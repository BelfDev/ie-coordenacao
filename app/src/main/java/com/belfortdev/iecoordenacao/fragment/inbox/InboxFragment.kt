package com.belfortdev.iecoordenacao.fragment.inbox

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import com.belfortdev.iecoordenacao.R
import com.belfortdev.iecoordenacao.activity.MessageActivity
import com.belfortdev.iecoordenacao.data.model.domain.Message
import com.belfortdev.iecoordenacao.fragment.BaseFragment
import com.belfortdev.iecoordenacao.viewmodel.MessageViewModel
import kotlinx.android.synthetic.main.activity_inbox.*
import kotlinx.android.synthetic.main.fragment_inbox.*

class InboxFragment : BaseFragment(), MessageListAdapter.MessageInteractor {

    companion object {
        fun newInstance() = InboxFragment()
    }

    private var messageViewModel: MessageViewModel? = null
    private var adapter = MessageListAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        initUI()
//        initiateDataListener()
    }

    override fun onStart() {
        super.onStart()
        initiateDataListener()
    }

    private fun initViewModel() {
        messageViewModel = ViewModelProviders.of(this).get(MessageViewModel::class.java)
        messageViewModel?.let { lifecycle.addObserver(it) }
    }

    private fun initUI() {
        recycler_view.layoutManager = LinearLayoutManager(requireContext(), LinearLayout.VERTICAL, false)
        adapter.interactor = this
        recycler_view.adapter = adapter
        activity?.fab?.setOnClickListener { onFABClick() }
    }

    private fun initiateDataListener() {
        toggleLoading(true)
        messageViewModel?.getAllMessages()?.observe(this, Observer { messageList ->
            toggleLoading(false)
            if (messageList != null) {
                last_sent_tv.text = messageViewModel!!.getLastSentText(messageList)
                adapter.setData(messageList)
            } else {
                showDialogAlert(message = "Occoreu um erro ao buscar as mensagens")
            }
        })
    }

    private fun onFABClick() {
        startActivity(context?.let { MessageActivity.newActivityIntent(it) })
    }

    override fun onMessageCardClicked(message: Message) {
        startActivity(MessageActivity.newActivityFromMessageIntent(requireContext(), message))
    }

    override fun getFragmentLayoutResource() = R.layout.fragment_inbox
}