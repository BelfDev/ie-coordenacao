package com.belfortdev.iecoordenacao.fragment

import android.app.Activity.RESULT_OK
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.belfortdev.iecoordenacao.R
import com.belfortdev.iecoordenacao.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.fragment_login.*


class LoginFragment : BaseFragment() {

    companion object {
        private const val SUCCESSFUL_LOGIN = 200
        private const val RESULT = "RESULT"

        fun newInstance() = LoginFragment()
    }

    private var userViewModel: UserViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    private fun initViewModel() {
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        userViewModel?.let { lifecycle.addObserver(it) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI() {
        username_et.setText(userViewModel?.populateWithLastUsername())
        username_et.setSelection(username_et.text.length)
        username_et.setOnEditorActionListener(onUsernameInput())
        password_et.setOnEditorActionListener(onPasswordInput())
        login_btn.setOnClickListener { onLoginButtonPress() }
    }

    private fun onUsernameInput(): TextView.OnEditorActionListener =
            TextView.OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    userViewModel?.usernameInput = v.text.toString()
                    password_et.requestFocus()
                    true
                } else {
                    false
                }
            }

    private fun onPasswordInput(): TextView.OnEditorActionListener =
            TextView.OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    userViewModel?.passwordInput = v.text.toString()
                    onLoginButtonPress()
                    true
                } else {
                    false
                }
            }

    private fun onLoginButtonPress() {
        view?.hideKeyboard()
        userViewModel?.usernameInput = username_et.text.toString()
        userViewModel?.passwordInput = password_et.text.toString()

        toggleLoading(true)
        if (userViewModel?.shouldPerformLogin()!!) {
            userViewModel?.logUserIn()?.observe(this, Observer { user ->
                toggleLoading(false)
                if (user != null) {
                    val result = Intent()
                    result.putExtra(RESULT, SUCCESSFUL_LOGIN)
                    activity?.setResult(RESULT_OK, result)
                    activity?.finish()
                } else {
                    showDialogAlert(message = "Não foi possível efetuar login", positiveClickListener = onPositiveButtonClick())
                }
            })
        } else {
            toggleLoading(false)
            showDialogAlert(message = "Por favor preencha todos os campos", positiveClickListener = onPositiveButtonClick())
        }
    }

    private fun onPositiveButtonClick(): DialogInterface.OnClickListener {
        return DialogInterface.OnClickListener { dialog, _ ->
            userViewModel?.passwordInput = ""
            password_et.setText("")
            dialog.dismiss()
        }
    }

    override fun getFragmentLayoutResource() = R.layout.fragment_login
}