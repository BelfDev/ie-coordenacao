package com.belfortdev.iecoordenacao.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.AppCompatTextView
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.belfortdev.iecoordenacao.R
import com.belfortdev.iecoordenacao.activity.MessageActivity.Companion.MESSAGE_READ
import com.belfortdev.iecoordenacao.data.model.domain.Data
import com.belfortdev.iecoordenacao.data.model.domain.Message
import com.belfortdev.iecoordenacao.viewmodel.MessageViewModel
import kotlinx.android.synthetic.main.composing_card.view.*
import kotlinx.android.synthetic.main.fragment_composition.*
import kotlinx.android.synthetic.main.interactable_message_card.view.*

class CompositionFragment : BaseFragment() {

    companion object {
        private const val INSTITUTE_OPTION = "Escola"

        fun newInstance() = CompositionFragment()
    }

    private var messageViewModel: MessageViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        initViewModel()
    }

    private fun initViewModel() {
        messageViewModel = ViewModelProviders.of(this).get(MessageViewModel::class.java)
        messageViewModel?.let { lifecycle.addObserver(it) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI() {
        initFirstSelectionSpinner()
        first_selection.interactable_card.setOnClickListener { onFirstSelectionCardClick() }
        second_selection.interactable_card.setOnClickListener { onSecondSelectionCardClick() }
        composition.box.setOnClickListener { onComposeMessageClick() }

        populateFieldsIfNeeded()
    }

    private fun populateFieldsIfNeeded() {
        if (arguments != null) {
            if (arguments!!.containsKey(MESSAGE_READ)) {
                val writtenMessage = arguments!![MESSAGE_READ] as Message
                val firstSelection = messageViewModel?.interpretSelection(writtenMessage)
                messageViewModel?.messageContent = writtenMessage.content

                composition.edit_text.setText(writtenMessage.content)
                composition.edit_text.setSelection(writtenMessage.content.length)
                firstSelection?.let { setSpinnerSelections(it) }
            }
        }
    }

    private fun setSpinnerSelections(firstSelection: Data.FirstSelection) {
        when (firstSelection) {
            Data.FirstSelection.INSTITUTE -> {
                first_selection.spinner.setSelection(0)
            }
            Data.FirstSelection.GRADE -> {
                first_selection.spinner.setSelection(1)
                second_selection.spinner.setSelection(Data.grades.indexOf(messageViewModel?.secondSelection))
            }
            Data.FirstSelection.CLASSNAME -> {
                first_selection.spinner.setSelection(2)
                second_selection.spinner.setSelection(Data.classNames.indexOf(messageViewModel?.secondSelection))
            }
        }
    }

    private fun initFirstSelectionSpinner() {
        val adapter = ArrayAdapter(activity, R.layout.item_spinner, Data.getFirstSelectionList())
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        first_selection.spinner.adapter = adapter
        first_selection.spinner.onItemSelectedListener = onFirstSelection()
    }

    private fun initSecondSelectionSpinner() {
        val adapter = ArrayAdapter(activity, R.layout.item_spinner, messageViewModel?.getSecondSpinnerDataSource())
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        second_selection.spinner.adapter = adapter
        second_selection.spinner.onItemSelectedListener = onSecondSelection()
    }

    private fun onFirstSelectionCardClick() {
        first_selection.spinner.performClick()
    }

    private fun onSecondSelectionCardClick() {
        first_selection.spinner.performClick()
    }

    private fun onFirstSelection(): AdapterView.OnItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    val selectedOption = (p1 as AppCompatTextView).text
                    handleFirstSelection(selectedOption.toString())
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {}
            }

    private fun handleFirstSelection(selectedOption: String) {
        val option = Data.FirstSelection.ptValueOf(selectedOption)
        messageViewModel?.firstSelection = option
        initSecondSelectionSpinner()
        second_selection.visibility = if (selectedOption != INSTITUTE_OPTION) VISIBLE else GONE
    }

    private fun onSecondSelection(): AdapterView.OnItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    val selectedOption = (p1 as AppCompatTextView).text
                    messageViewModel?.secondSelection = selectedOption.toString()
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {}
            }

    private fun onComposeMessageClick() {
        composition.edit_text.requestFocus()
        view?.showKeyboard()
    }

    private fun onSendMessageClick() {
        messageViewModel?.messageContent = composition.edit_text.text.toString()
        toggleLoading(true)
        messageViewModel?.sendMessage()?.observe(this, Observer { message ->
            toggleLoading(false)
            if (message != null) {
                view?.hideKeyboard()
                activity?.finish()
            } else {
                showDialogAlert(message = "Erro ao tentar enviar mensagem")
            }
        })

        if (messageViewModel?.messageContent.isNullOrBlank()) {
            toggleLoading(false)
            showDialogAlert(message = "Não esqueça de escrever o conteúdo da mensagem")
        }
    }

    private fun onDiscardMessageClick() {
        messageViewModel?.eraseAllMessageInfo()
        activity?.finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.send_menu_item -> {
                onSendMessageClick()
                return true
            }
            R.id.discard_menu_item -> {
                onDiscardMessageClick()
                return true
            }
        }
        return false
    }

    override fun getFragmentLayoutResource() = R.layout.fragment_composition

}