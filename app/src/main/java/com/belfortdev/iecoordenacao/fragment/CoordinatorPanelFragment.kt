package com.belfortdev.iecoordenacao.fragment

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import com.belfortdev.iecoordenacao.R
import com.belfortdev.iecoordenacao.activity.InboxActivity
import com.belfortdev.iecoordenacao.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.fragment_coordinator_panel.*


class CoordinatorPanelFragment: BaseFragment() {

    companion object {
        fun newInstance() = CoordinatorPanelFragment()
    }

    private var userViewModel: UserViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    private fun initViewModel() {
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        userViewModel?.let { lifecycle.addObserver(it) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI() {
        username_tv.text = userViewModel?.sessionManager?.getSenderName()
        messages_card.setOnClickListener { onMessagesOptionPress() }
    }

    private fun onMessagesOptionPress() {
        startActivity(context?.let { InboxActivity.newActivityIntent(it) })
    }

    override fun getFragmentLayoutResource() = R.layout.fragment_coordinator_panel
}