package com.belfortdev.iecoordenacao.viewmodel

import android.arch.lifecycle.*
import com.belfortdev.iecoordenacao.application.IEApplication
import com.belfortdev.iecoordenacao.data.model.domain.Data
import com.belfortdev.iecoordenacao.data.model.domain.Message
import com.belfortdev.iecoordenacao.data.model.request.CreateMessageRequest
import com.belfortdev.iecoordenacao.data.repository.MessageRepository
import com.belfortdev.iecoordenacao.manager.SessionManager
import io.reactivex.disposables.CompositeDisposable
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class MessageViewModel : ViewModel(), LifecycleObserver {

    @Inject
    lateinit var messageRepository: MessageRepository
    @Inject
    lateinit var sessionManager: SessionManager

    private val compositeDisposable = CompositeDisposable()
    private var messageLiveData: LiveData<Message>? = null
    private var messageListLiveData: LiveData<List<Message>>? = null

    var firstSelection: Data.FirstSelection? = null
    var secondSelection: String? = null
    var messageContent: String? = null

    private var classNameSelection: String? = null
    private var gradeSelection: String? = null

    init {
        initializeDagger()
    }

    fun getAllMessages(): LiveData<List<Message>> {
        messageListLiveData = null
        messageListLiveData = MutableLiveData<List<Message>>()
        messageListLiveData = messageRepository.getAllMessages(where = "{\"from\":\"${sessionManager.getSenderName()}\"}")
        return messageListLiveData as LiveData<List<Message>>
    }

    private fun createMessage(messageRequest: CreateMessageRequest): MutableLiveData<Message> {
        messageLiveData = null
        messageLiveData = MutableLiveData<Message>()
        messageLiveData = messageRepository.createMessage(sessionManager.getSessionId(), messageRequest)
        return messageLiveData as MutableLiveData<Message>
    }

    fun sendMessage(): LiveData<Message>? {
        var result: MutableLiveData<Message>? = null
        if (shouldSendMessage()) {
            interpretSecondSelection()
            val messageRequest = CreateMessageRequest(grade = gradeSelection, className = classNameSelection, from = sessionManager.getSenderName(), content = messageContent!!)
            result = createMessage(messageRequest)
        }
        return result
    }

    private fun shouldSendMessage(): Boolean {
        return (firstSelection != null && !messageContent.isNullOrBlank())
    }

    private fun interpretSecondSelection() {
        if (Data.grades.contains(secondSelection)) {
            gradeSelection = secondSelection
        } else if (Data.classNames.contains(secondSelection)) {
            classNameSelection = secondSelection
        }
    }

    fun eraseAllMessageInfo() {
        firstSelection = null
        secondSelection = null
        messageContent = null
        classNameSelection = null
        gradeSelection = null
    }

    private fun getInstitute(): String = Data.institute

    private fun getAllGrades(): ArrayList<String> = Data.grades

    private fun getAllClassNames(): ArrayList<String> = Data.classNames

    fun getSecondSpinnerDataSource(): ArrayList<String> {
        return if (firstSelection != null) {
            when (firstSelection) {
                Data.FirstSelection.GRADE -> getAllGrades()
                Data.FirstSelection.CLASSNAME -> getAllClassNames()
                else -> {
                    ArrayList()
                }
            }
        } else ArrayList()
    }

    fun interpretSelection(message: Message): Data.FirstSelection {
        when {
            message.className != null -> {
                firstSelection = Data.FirstSelection.CLASSNAME
                secondSelection = message.className
                return Data.FirstSelection.CLASSNAME
            }
            message.grade != null -> {
                firstSelection = Data.FirstSelection.GRADE
                secondSelection = message.grade
                return Data.FirstSelection.GRADE
            }
            else -> {
                firstSelection = Data.FirstSelection.INSTITUTE
                return Data.FirstSelection.INSTITUTE
            }
        }
    }

    fun getLastSentText(messageList: List<Message>?): String {
        return if (messageList?.isNotEmpty()!!) {
            val lastSentDate = messageList.first().createdAt
            val formatedDate = formatDate(lastSentDate)
            "Último envio: $formatedDate"
        } else {
            "Nenhuma mensagem enviada."
        }
    }

    private fun formatDate(date: Date): String {
        return SimpleDateFormat("d 'de' MMMM").format(date)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun unSubscribeViewModel() {
        for (disposable in messageRepository.allCompositeDisposable) {
            compositeDisposable.addAll(disposable)
        }
        compositeDisposable.clear()
    }

    override fun onCleared() {
        unSubscribeViewModel()
        super.onCleared()
    }

    private fun initializeDagger() = IEApplication.appComponent.inject(this)
}