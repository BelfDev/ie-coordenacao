package com.belfortdev.iecoordenacao.viewmodel

import android.arch.lifecycle.*
import com.belfortdev.iecoordenacao.application.IEApplication
import com.belfortdev.iecoordenacao.data.model.domain.User
import com.belfortdev.iecoordenacao.data.model.response.Meta
import com.belfortdev.iecoordenacao.data.repository.UserRepository
import com.belfortdev.iecoordenacao.manager.SessionManager
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class UserViewModel: ViewModel(), LifecycleObserver {

    @Inject lateinit var userRepository: UserRepository
    @Inject lateinit var sessionManager: SessionManager

    private val compositeDisposable = CompositeDisposable()

    private var isUserLoggedOut: LiveData<Boolean>? = null
    private var userLiveData: LiveData<User>? = null


    var usernameInput: String? = null
    var passwordInput: String? = null

    init {
        initializeDagger()
    }

    fun logUserIn(): LiveData<User>? {
        userLiveData = null
        if (shouldPerformLogin()) {
            userLiveData = null
            userLiveData = MutableLiveData<User>()
            userLiveData = userRepository.logUserIn(usernameInput!!, passwordInput!!)
        }
        return userLiveData
    }

    fun logUserOut(): LiveData<Boolean> {
        isUserLoggedOut = null
        isUserLoggedOut = MutableLiveData<Boolean>()
        isUserLoggedOut = userRepository.logUserOut()

        sessionManager.eraseUseInfo()

        return isUserLoggedOut as LiveData<Boolean>
    }

    fun shouldDisplayAuthentication(): Boolean {
          return !sessionManager.isUserLoggedIn()
    }

    fun shouldPerformLogin(): Boolean {
        return !usernameInput.isNullOrBlank() && !passwordInput.isNullOrBlank()
    }

    fun populateWithLastUsername(): String? {
        usernameInput = sessionManager.getLastUserName()
        return usernameInput
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun unSubscribeViewModel() {
        for (disposable in userRepository.allCompositeDisposable) {
            compositeDisposable.addAll(disposable)
        }
        compositeDisposable.clear()
    }

    override fun onCleared() {
        unSubscribeViewModel()
        super.onCleared()
    }

    private fun initializeDagger() = IEApplication.appComponent.inject(this)
}