package com.belfortdev.iecoordenacao.data.model.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class UserResponse(@JsonProperty("id")
                        val id: String = "",
                        @JsonProperty("created_at")
                        val createdAt: Date,
                        @JsonProperty("updated_at")
                        val updatedAt: Date,
                        @JsonProperty("confirmed_at")
                        val confirmedAt: Date,
                        @JsonProperty("username")
                        val username: String = "",
                        @JsonProperty("email")
                        val email: String = "",
                        @JsonProperty("role")
                        val role: String = "",
                        @JsonProperty("admin")
                        val admin: String = "",
                        @JsonProperty("stats")
                        val stats: Stats,
                        @JsonProperty("custom_fields")
                        val customFields: CustomUserFields,
                        @JsonProperty("friend_counts")
                        val friendCounts: FriendCounts)