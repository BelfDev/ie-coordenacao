package com.belfortdev.iecoordenacao.data.model.request

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class CreateMessageRequest(@JsonProperty("institute")
                                val institute: String = "Equipe1",
                                @JsonProperty("grade")
                                val grade: String? = "",
                                @JsonProperty("classname")
                                val className: String? = "",
                                @JsonProperty("from")
                                val from: String = "",
                                @JsonProperty("content")
                                val content: String = "")