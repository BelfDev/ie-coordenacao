package com.belfortdev.iecoordenacao.data.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.belfortdev.iecoordenacao.data.datasource.AppceleratorDataSource
import com.belfortdev.iecoordenacao.data.model.domain.User
import com.belfortdev.iecoordenacao.data.model.response.FullResponse
import com.belfortdev.iecoordenacao.data.model.response.Meta
import com.belfortdev.iecoordenacao.manager.SessionManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class UserRepository @Inject constructor(
        private val appceleratorDataSource: AppceleratorDataSource,
        private val sessionManager: SessionManager
) {

    val allCompositeDisposable: MutableList<Disposable> = arrayListOf()

    fun logUserIn(username: String, password: String): LiveData<User> {
        val mutableLiveData = MutableLiveData<User>()
        val disposable = appceleratorDataSource.logUserIn(username, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ fullResponse ->
                    val user = User(fullResponse.response?.userList!![0])
                    fullResponse.meta.sessionId?.let { sessionManager.saveUserInfo(user, it) }
                    mutableLiveData.value = user
                }, { error ->
                    error!!.printStackTrace()
                    mutableLiveData.value = null
                })

        allCompositeDisposable.add(disposable)
        return mutableLiveData
    }

    fun logUserOut(): LiveData<Boolean> {
        val mutableLiveData = MutableLiveData<Boolean>()
        val disposable = appceleratorDataSource.logUserOut()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ meta ->
                    mutableLiveData.value = meta.code == 200
                }, { error ->
                    error!!.printStackTrace()
                    mutableLiveData.value = false
                })
        allCompositeDisposable.add(disposable)
        return mutableLiveData
    }

}