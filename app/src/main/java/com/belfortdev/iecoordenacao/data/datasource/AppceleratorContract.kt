package com.belfortdev.iecoordenacao.data.datasource

class AppceleratorContract {
    companion object {
        // Configs
        const val BASE_API_URL = "https://api.cloud.appcelerator.com/v1/"
        const val ACCESS_KEY_VALUE = "rhRa5onuU9Q0s6tWUXmbUzAEor6hkwxt"

        // Parameters
        const val ACCESS_KEY_PARAM = "key"
        const val PRETTY_JSON_PARAM = "pretty_json"
        const val SESSION_ID_PARAM = "_session_id"
        const val FIELDS_PARAM = "fields"
        const val LOGIN_PARAM = "login"
        const val PASSWORD_PARAM = "password"
        const val ORDER_PARAM = "order"
        const val WHERE_PARAM = "where"
        const val LIMIT_PARAM = "limit"

        // Default Values
        const val WHERE_DEFAULT_VALUE = ""
        const val ORDER_DEFAULT_VALUE = "-created_at"
        const val LIMIT_DEFAULT_VALUE = 60

        // Endpoints
        const val CREATE_MESSAGE = "objects/message/create.json"
        const val USER_LOGIN = "users/login.json"
        const val USER_LOGOUT = "users/logout.json"
        const val MESSAGES = "objects/message/query.json"

    }
}