package com.belfortdev.iecoordenacao.data.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.belfortdev.iecoordenacao.data.datasource.AppceleratorContract
import com.belfortdev.iecoordenacao.data.datasource.AppceleratorDataSource
import com.belfortdev.iecoordenacao.data.datasource.RoomMessageDataSource
import com.belfortdev.iecoordenacao.data.model.domain.Message
import com.belfortdev.iecoordenacao.data.model.domain.User
import com.belfortdev.iecoordenacao.data.model.entity.MessageEntity
import com.belfortdev.iecoordenacao.data.model.request.CreateMessageRequest
import com.belfortdev.iecoordenacao.data.model.response.FullResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MessageRepository @Inject constructor(
        private val appceleratorDataSource: AppceleratorDataSource,
        private val roomMessageDataSource: RoomMessageDataSource
) {

    val allCompositeDisposable: MutableList<Disposable> = arrayListOf()

    fun getAllMessages(where: String = AppceleratorContract.WHERE_DEFAULT_VALUE,
                       order: String = AppceleratorContract.ORDER_DEFAULT_VALUE,
                       limit: Int = AppceleratorContract.LIMIT_DEFAULT_VALUE): LiveData<List<Message>> {
        val mutableLiveData = MutableLiveData<List<Message>>()
        val disposable = appceleratorDataSource.getAllMessages(where, order, limit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ messageList ->
                    mutableLiveData.value = messageList
                }, { error ->
                    mutableLiveData.value = null
                    error!!.printStackTrace()
                })
        allCompositeDisposable.add(disposable)
        return mutableLiveData
    }

    private fun convert(entityList: List<MessageEntity>): List<Message> =
            entityList.map { entity -> Message(entity) }

    fun createMessage(sessionId: String, createMessageRequest: CreateMessageRequest): LiveData<Message> {
        val mutableLiveData = MutableLiveData<Message>()
        val disposable = appceleratorDataSource.createMessage(sessionId, createMessageRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ apsFullResponse ->
                    mutableLiveData.value = Message(apsFullResponse.response?.messageList!![0])
                }, { error ->
                    mutableLiveData.value = null
                    error!!.printStackTrace()
                })

        allCompositeDisposable.add(disposable)
        return mutableLiveData
    }
}