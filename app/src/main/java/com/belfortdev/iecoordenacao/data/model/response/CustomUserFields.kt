package com.belfortdev.iecoordenacao.data.model.response

import com.fasterxml.jackson.annotation.JsonProperty

data class CustomUserFields(@JsonProperty("sender_name")
                            val senderName: String = "")