package com.belfortdev.iecoordenacao.data.model.domain

import com.belfortdev.iecoordenacao.data.model.response.UserResponse
import java.util.*

class User(userResponse: UserResponse) {
    val id: String = userResponse.id
    val createdAt: Date = userResponse.createdAt
    val updateAt: Date = userResponse.updatedAt
    val username: String = userResponse.username
    val email: String = userResponse.email
    val role: String = userResponse.role
    val admin: String = userResponse.admin
    val senderName:String = userResponse.customFields.senderName
}