package com.belfortdev.iecoordenacao.data.model.response

import com.fasterxml.jackson.annotation.JsonProperty

data class Photos(@JsonProperty("total_count")
                  val totalCount: Int = 0)