package com.belfortdev.iecoordenacao.data.model.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class MessageResponse(@JsonProperty("user_id")
                           val userId: String = "",
                           @JsonProperty("created_at")
                           val createdAt: Date,
                           @JsonProperty("updated_at")
                           val updatedAt: Date,
                           @JsonProperty("institute")
                           val institute: String = "Equipe1",
                           @JsonProperty("grade")
                           val grade: String? = "",
                           @JsonProperty("classname")
                           val className: String? = "",
                           @JsonProperty("from")
                           val from: String = "",
                           @JsonProperty("content")
                           val content: String = "",
                           @JsonProperty("id")
                           val id: String = "")