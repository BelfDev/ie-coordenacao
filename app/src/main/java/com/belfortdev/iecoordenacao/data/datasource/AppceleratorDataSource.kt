package com.belfortdev.iecoordenacao.data.datasource

import android.content.SharedPreferences
import com.belfortdev.iecoordenacao.data.model.domain.Message
import com.belfortdev.iecoordenacao.data.model.domain.User
import com.belfortdev.iecoordenacao.data.model.request.CreateMessageRequest
import com.belfortdev.iecoordenacao.data.model.response.FullResponse
import com.belfortdev.iecoordenacao.data.model.response.Meta
import com.belfortdev.iecoordenacao.manager.SessionManager
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.reactivex.Observable
import javax.inject.Inject

class AppceleratorDataSource @Inject constructor(private val appceleratorService: AppceleratorService) {

    fun logUserIn(username: String, password: String): Observable<FullResponse> =
            appceleratorService.logUserIn(username, password)

    fun logUserOut(): Observable<Meta> =
            appceleratorService.logUserOut()
                    .map { fullResponse -> fullResponse.meta }

    fun getAllMessages(where: String?, order: String?, limit: Int?): Observable<List<Message>?> =
            appceleratorService.getAllMessages(where, order, limit)
                    .map { fullResponse ->
                        fullResponse.response?.messageList?.map { element ->
                            Message(element)
                        }
                    }

    fun createMessage(sessionId: String, createMessageRequest: CreateMessageRequest) =
            appceleratorService.createMessage(sessionId, jacksonObjectMapper().writeValueAsString(createMessageRequest))

}