package com.belfortdev.iecoordenacao.data.model.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class Response(@JsonProperty("message")
                    val messageList: List<MessageResponse>?,

                    @JsonProperty("users")
                    val userList: List<UserResponse>?)