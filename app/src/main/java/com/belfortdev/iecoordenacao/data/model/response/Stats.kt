package com.belfortdev.iecoordenacao.data.model.response

import com.fasterxml.jackson.annotation.JsonProperty

data class Stats(@JsonProperty("photos")
                 val photos: Photos,
                 @JsonProperty("storage")
                 val storage: Storage)