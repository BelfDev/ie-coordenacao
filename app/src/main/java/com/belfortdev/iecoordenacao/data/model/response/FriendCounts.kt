package com.belfortdev.iecoordenacao.data.model.response

import com.fasterxml.jackson.annotation.JsonProperty

data class FriendCounts(@JsonProperty("requests")
                        val requests: Int = 0,
                        @JsonProperty("friends")
                        val friends: Int = 0)