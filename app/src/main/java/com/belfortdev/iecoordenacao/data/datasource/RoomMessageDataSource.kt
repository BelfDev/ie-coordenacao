package com.belfortdev.iecoordenacao.data.datasource

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.belfortdev.iecoordenacao.data.model.entity.MessageEntity
import com.belfortdev.iecoordenacao.util.Converters

@Database(entities = [(MessageEntity::class)], version = 1)
@TypeConverters(Converters::class)
abstract class RoomMessageDataSource : RoomDatabase() {

    abstract fun messageDao(): MessageDao

    companion object {
        fun buildPersistentCurrency(context: Context): RoomMessageDataSource = Room.databaseBuilder(
                context.applicationContext,
                RoomMessageDataSource::class.java,
                RoomContract.DATABASE_MESSAGE
        ).build()
    }

}