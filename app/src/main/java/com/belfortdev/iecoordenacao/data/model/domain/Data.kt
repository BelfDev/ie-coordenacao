package com.belfortdev.iecoordenacao.data.model.domain

class Data {
    companion object {
        const val institute: String = "Equipe1"

        val grades = arrayListOf("1ª SÉRIE",
                "1º ANO",
                "2ª SÉRIE",
                "2º ANO",
                "3ª SÉRIE",
                "3º ANO",
                "4º ANO",
                "5º ANO",
                "6º ANO",
                "7º ANO",
                "8º ANO",
                "9º ANO",
                "BALLET",
                "CAPOEIRA",
                "EJA - ENSINO MÉDIO IPA SUPLETIVO",
                "EJA - FUNDAMENTAL SUPLETIVO",
                "FUTSAL",
                "GINASTICA RITMICA",
                "INATIVO",
                "JARDIM I",
                "JARDIM II",
                "JARDIM III",
                "JIU JITSU",
                "JUDO",
                "MATERNAL I",
                "MATERNAL II",
                "PRÉ-VESTIBULAR",
                "PRÉ-VESTIBULAR-S")

        val classNames = arrayListOf("1001",
                "101",
                "102",
                "2001",
                "201",
                "202",
                "3001",
                "301",
                "302",
                "401",
                "402",
                "501",
                "502",
                "601",
                "602",
                "7 FASE - 1SEM-N",
                "7 FASE - 2SEM-N",
                "701",
                "8 FASE - 1SEM-N",
                "8 FASE - 2SEM-N",
                "801",
                "901",
                "BALLET",
                "CAPOEIRA",
                "FUTSAL",
                "FUTSAL-2467",
                "FUTSAL-2478",
                "FUTSAL-3567",
                "FUTSAL-3578",
                "FUTSAL-667",
                "GINASTICA RITMICA",
                "INATIVO",
                "IPA-N",
                "IPA-N2",
                "IPA-N3",
                "IPA-N4",
                "JARDIM II -T (B)",
                "JDM I - M",
                "JDM I - T",
                "JDM I - T (B)",
                "JDM II - M",
                "JDM II -T",
                "JDT III",
                "JUDO",
                "JUI JITSU",
                "MAT I ",
                "MAT II - A",
                "MAT II - B",
                "PRÉ-VESTIBULAR-S",
                "PV / M",
                "PV / N",
                "PV / SAB",
                "PV / T",
                "TREINAMENTO")

        fun getFirstSelectionList(): ArrayList<String> {
            val selectionList = mutableListOf<String>()
            for (enum in FirstSelection.values()) {
                selectionList.add(enum.ptName)
            }
            return selectionList as ArrayList<String>
        }
    }

    enum class FirstSelection(val ptName: String) {
        INSTITUTE("Escola"),
        GRADE("Série"),
        CLASSNAME("Turma");

        companion object {
            fun ptValueOf(string: String): FirstSelection {
                for (enum in FirstSelection.values()) {
                    if (enum.ptName == string) {
                        return enum
                    }
                }
                throw Exception("No enum associated with the provided portuguese value")
            }
        }
    }
}