package com.belfortdev.iecoordenacao.data.model.domain

import com.belfortdev.iecoordenacao.data.model.entity.MessageEntity
import com.belfortdev.iecoordenacao.data.model.response.MessageResponse
import java.io.Serializable
import java.util.*

data class Message(val id: String,
                   val userId: String,
                   val createdAt: Date,
                   val updatedAt: Date,
                   val institute: String,
                   val grade: String?,
                   val className: String?,
                   val from: String,
                   val content: String) : Serializable {

    constructor(messageResponse: MessageResponse) : this(
            id = messageResponse.id,
            userId = messageResponse.userId,
            createdAt = messageResponse.createdAt,
            updatedAt = messageResponse.updatedAt,
            institute = messageResponse.institute,
            grade = messageResponse.grade,
            className = messageResponse.className,
            from = messageResponse.from,
            content = messageResponse.content)

    constructor(entity: MessageEntity) : this(
            id = entity.id.toString(),
            userId = entity.userId,
            createdAt = entity.createdAt,
            updatedAt = entity.updateAt,
            institute = entity.institute,
            grade = entity.grade,
            className = entity.className,
            from = entity.from,
            content = entity.content)

}