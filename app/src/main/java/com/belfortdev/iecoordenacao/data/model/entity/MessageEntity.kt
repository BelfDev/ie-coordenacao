package com.belfortdev.iecoordenacao.data.model.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.belfortdev.iecoordenacao.data.datasource.RoomContract
import java.util.*

@Entity(tableName = RoomContract.TABLE_MESSAGES)
data class MessageEntity (
        @PrimaryKey(autoGenerate = true) val id: Long,
        val userId: String,
        val createdAt: Date,
        val updateAt: Date,
        val institute: String,
        val grade: String?,
        val className: String?,
        val from: String,
        val content: String
)