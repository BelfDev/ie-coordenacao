package com.belfortdev.iecoordenacao.data.datasource

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.belfortdev.iecoordenacao.data.model.entity.MessageEntity
import io.reactivex.Flowable

@Dao
interface MessageDao {

    @Query(RoomContract.SELECT_CURRENCIES_COUNT)
    fun getNumberOfMessages(): Flowable<Int>

    @Insert
    fun insertAll(currencies: List<MessageEntity>)

    @Query(RoomContract.SELECT_CURRENCIES)
    fun getAllMessages(): Flowable<List<MessageEntity>>

}