package com.belfortdev.iecoordenacao.data.model.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class Meta(@JsonProperty("code")
                val code: Int = 0,
                @JsonProperty("status")
                val status: String = "",
                @JsonProperty("method_name")
                val methodName: String? = "",
                @JsonProperty("session_id")
                val sessionId: String? = "",
                @JsonProperty("cc_code")
                val ccCode: String? = "",
                @JsonProperty("message")
                val message: String? = "")