package com.belfortdev.iecoordenacao.data.datasource

import com.belfortdev.iecoordenacao.data.datasource.AppceleratorContract.Companion.CREATE_MESSAGE
import com.belfortdev.iecoordenacao.data.datasource.AppceleratorContract.Companion.FIELDS_PARAM
import com.belfortdev.iecoordenacao.data.datasource.AppceleratorContract.Companion.LIMIT_PARAM
import com.belfortdev.iecoordenacao.data.datasource.AppceleratorContract.Companion.LOGIN_PARAM
import com.belfortdev.iecoordenacao.data.datasource.AppceleratorContract.Companion.MESSAGES
import com.belfortdev.iecoordenacao.data.datasource.AppceleratorContract.Companion.ORDER_PARAM
import com.belfortdev.iecoordenacao.data.datasource.AppceleratorContract.Companion.PASSWORD_PARAM
import com.belfortdev.iecoordenacao.data.datasource.AppceleratorContract.Companion.SESSION_ID_PARAM
import com.belfortdev.iecoordenacao.data.datasource.AppceleratorContract.Companion.USER_LOGIN
import com.belfortdev.iecoordenacao.data.datasource.AppceleratorContract.Companion.USER_LOGOUT
import com.belfortdev.iecoordenacao.data.datasource.AppceleratorContract.Companion.WHERE_PARAM
import com.belfortdev.iecoordenacao.data.model.response.FullResponse
import io.reactivex.Observable
import retrofit2.http.*


interface AppceleratorService {

    @FormUrlEncoded
    @POST(USER_LOGIN)
    fun logUserIn(
            @Field(LOGIN_PARAM) username: String,
            @Field(PASSWORD_PARAM) password: String
    ): Observable<FullResponse>

    @GET(USER_LOGOUT)
    fun logUserOut(): Observable<FullResponse>

    @GET(MESSAGES)
    fun getAllMessages(
            @Query(WHERE_PARAM) where: String?,
            @Query(ORDER_PARAM) order: String?,
            @Query(LIMIT_PARAM) limit: Int?
    ): Observable<FullResponse>

    @FormUrlEncoded
    @POST(CREATE_MESSAGE)
    fun createMessage(
            @Query(SESSION_ID_PARAM) sessionId: String,
            @Field(FIELDS_PARAM) createMessageRequestJSON: String
    ): Observable<FullResponse>

}