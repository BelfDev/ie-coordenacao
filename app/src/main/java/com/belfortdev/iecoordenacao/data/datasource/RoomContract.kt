package com.belfortdev.iecoordenacao.data.datasource

class RoomContract {

    companion object {
        const val DATABASE_MESSAGE = "message.db"

        const val TABLE_MESSAGES = "messages"

        private const val SELECT_COUNT = "SELECT COUNT(*) FROM "
        private const val SELECT_FROM = "SELECT * FROM "

        const val SELECT_CURRENCIES_COUNT = SELECT_COUNT + TABLE_MESSAGES
        const val SELECT_CURRENCIES = SELECT_FROM + TABLE_MESSAGES
    }
}