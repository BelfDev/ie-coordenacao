package com.belfortdev.iecoordenacao.data.model.response

import com.fasterxml.jackson.annotation.JsonProperty

data class Storage(@JsonProperty("used")
                   val used: Int = 0)