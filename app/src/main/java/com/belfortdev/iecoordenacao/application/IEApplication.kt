package com.belfortdev.iecoordenacao.application

import android.app.Application
import android.support.multidex.MultiDexApplication
import com.belfortdev.iecoordenacao.di.AppComponent
import com.belfortdev.iecoordenacao.di.AppModule
import com.belfortdev.iecoordenacao.di.DaggerAppComponent
import com.belfortdev.iecoordenacao.di.RemoteModule

class IEApplication: MultiDexApplication() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initializeDagger()
    }

    fun initializeDagger() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .remoteModule(RemoteModule())
                .build()
    }
}