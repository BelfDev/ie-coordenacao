package com.belfortdev.iecoordenacao.di

import android.content.Context
import com.belfortdev.iecoordenacao.application.IEApplication
import com.belfortdev.iecoordenacao.manager.SessionManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val ieApplication: IEApplication) {

    @Provides @Singleton
    fun provideContext(): Context = ieApplication

    @Provides @Singleton
    fun provideSessionManager(context: Context): SessionManager = SessionManager(context)

}