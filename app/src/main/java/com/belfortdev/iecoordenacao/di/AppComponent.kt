package com.belfortdev.iecoordenacao.di

import com.belfortdev.iecoordenacao.manager.SessionManager
import com.belfortdev.iecoordenacao.viewmodel.MessageViewModel
import com.belfortdev.iecoordenacao.viewmodel.UserViewModel
import dagger.Component
import javax.inject.Singleton

@Component(modules = [(AppModule::class), (RemoteModule::class), (RoomModule::class)])
@Singleton
interface AppComponent {

    fun inject(messageViewModel: MessageViewModel)

    fun inject(userViewModel: UserViewModel)

    fun inject(sessionManager: SessionManager)
}