package com.belfortdev.iecoordenacao.di

import android.content.Context
import com.belfortdev.iecoordenacao.data.datasource.RoomMessageDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {

    @Provides @Singleton
    fun provideRoomMessageDataSource(context: Context) =
            RoomMessageDataSource.buildPersistentCurrency(context)
}